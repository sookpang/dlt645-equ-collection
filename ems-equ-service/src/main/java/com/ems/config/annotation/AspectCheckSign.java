package com.ems.config.annotation;

import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.Base64;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.alibaba.fastjson.JSONObject;
import com.ems.common.ServiceException;
import com.ems.enums.RespCode;

/**
 * 签名校验
 * 
 * @author chenzhao @date Oct 16, 2019
 */
@Aspect
@Component
public class AspectCheckSign {

	private Logger log = LoggerFactory.getLogger(AspectCheckSign.class);

//	@Pointcut("@within(com.ems.config.annotation.CheckSign)") // 匹配加注解的类/方法
//	public void checkSign() {
//	}

	/**
	 * 验签+可选验登陆
	 * 
	 * @author chenzhao @date Oct 18, 2019
	 * @param pjp
	 * @return
	 * @throws Throwable
	 */
	@Around("@annotation(CheckSign)")
	public Object around(ProceedingJoinPoint pjp) throws Throwable {
		long startTime = System.currentTimeMillis();
		Method method = ((MethodSignature) pjp.getSignature()).getMethod();
		CheckSign checkSign = ((MethodSignature) pjp.getSignature()).getMethod().getAnnotation(CheckSign.class);
		ServletRequestAttributes sra = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
		HttpServletRequest req = sra.getRequest();
		String sign = req.getHeader("sign");
		String token = req.getHeader("token");
		String timestamp = req.getHeader("timestamp");
		if (StringUtils.isEmpty(sign) || StringUtils.isEmpty(token) || StringUtils.isEmpty(timestamp)) {
			throw new ServiceException(RespCode.PARAM_INCOMPLETE, "sign,token,timestamp(http请求头中)");
		}
		Object[] args = pjp.getArgs();
		Parameter[] params = method.getParameters();
		for (int i = 0; i < args.length; i++) {
			Parameter parameter = params[i];
			StringBuffer s1 = new StringBuffer();
			if (parameter.isAnnotationPresent(RequestBody.class)) {
				JSONObject reqJson;
				if (args[i] instanceof JSONObject) {// JSONObject
					reqJson = (JSONObject) args[i];
				} else if (args[i] instanceof String) {// String
					reqJson = JSONObject.parseObject((String) args[i]);
				} else {// java Object
					reqJson = (JSONObject) JSONObject.toJSON(args[i]);
				}
				log.info(">>>check api sign,#sign:[{}],#token:[{}],#timestamp:[{}]\n#requestBody:[{}]", sign, token,
						timestamp, reqJson);
				TreeMap<String, Object> reqTreeMap = new TreeMap<>(reqJson.toJavaObject(Map.class));// TreeMap自动根据key升序排序
				for (Iterator iterator = reqTreeMap.entrySet().iterator(); iterator.hasNext();) {
					Entry entry = (Entry) iterator.next();
					s1.append(entry.getValue());
				}
				s1.append(timestamp);// 加时间戳
				String localSign = DigestUtils.md5DigestAsHex(s1.toString().getBytes());
				log.debug("#localSign:[{}] #未MD5前的原始字符串s1:[{}]", localSign, s1);
				if (!sign.equalsIgnoreCase(localSign)) {
					throw new ServiceException(RespCode.REQ_SIGN_ERROR);
				}
				log.debug("验签通过，开始校验登陆");
				if (checkSign.isCheckLogin()) {
					// TODO
				} else {
					log.debug("不校验登陆：{}", method.getName());
				}

			} else {
				log.debug("#未注解arg,[{}]:[{}]", params[i], args[i]);
			}
		}
		log.debug(">>>api proceedInterval,#api[{}],#interval:[{}]", method, System.currentTimeMillis() - startTime);
		// #go on
		Object obj = pjp.proceed();
		return obj;
	}

	public static String base64(String str) {
		String strBase64 = Base64.getEncoder().encodeToString(str.getBytes());
		System.out.println("#str加密后:" + strBase64);
		byte[] bytes = Base64.getDecoder().decode(strBase64);
		System.out.println("#str加解密后:" + new String(bytes));

		return null;
	}

	public static void main(String[] args) {
		base64("撒旦解放了手机发了几位老人家");
	}

}
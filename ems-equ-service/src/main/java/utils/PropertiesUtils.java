package utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Properties;

public final class PropertiesUtils {
	private static final Logger logger = LoggerFactory.getLogger(PropertiesUtils.class);
	// 文件路径
	private static final String[] CFG_PATHS = new String[] { "/application.properties", "/config.properties" };
	// 实例
	private static volatile PropertiesUtils instance;

	private Properties props = null;

	/*
	 * 刷新
	 */
	private synchronized void refresh() {
		if (null != props) {
			props.clear();
		}
		for (String path : CFG_PATHS) {
			try {
				if (null == props) {
					props = new Properties();
					props.load(PropertiesUtils.class.getResourceAsStream(path));
				}
			} catch (IOException e) {
				logger.error(e.getMessage(), e);
			}
		}
	}

	/*
	 * 检查实例
	 */
	private static void checkInstance() {
		if (instance == null) {
			synchronized (CFG_PATHS) {
				if (instance == null) {
					instance = new PropertiesUtils();
				}
			}
		}
	}

	public static String getProperty(String key) {
		checkInstance();
		return instance.props.getProperty(key);
	}

	public static String getProperty(String key, String defStr) {
		checkInstance();
		return instance.props.getProperty(key, defStr);
	}

	private PropertiesUtils() {
		super();
		refresh();
	}
}